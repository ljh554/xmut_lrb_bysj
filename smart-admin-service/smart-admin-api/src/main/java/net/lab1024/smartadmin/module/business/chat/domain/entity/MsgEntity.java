package net.lab1024.smartadmin.module.business.chat.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.lab1024.smartadmin.common.domain.BaseEntity;
import java.util.Date;
import lombok.Data;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:23:01
 * @since JDK1.8
 */
@Data
@TableName("chat_msg")
public class MsgEntity extends BaseEntity{

    /**
     * 发送者id
     */
    private String senduserid;

    /**
     * 接收者id
     */
    private String reciveuserid;

    /**
     * 发送内容
     */
    private String sendtext;

    /**
     * 发送时间
     */
    private Date date;

    /**
     * 消息类型
     */
    private String mine;

    /**
     * name
     */
    private String name;

    /**
     * img
     */
    private String img;



}
