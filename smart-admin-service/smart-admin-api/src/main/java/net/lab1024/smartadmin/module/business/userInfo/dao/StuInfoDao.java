package net.lab1024.smartadmin.module.business.userInfo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import net.lab1024.smartadmin.module.business.userInfo.domain.dto.StuInfoQueryDTO;
import net.lab1024.smartadmin.module.business.userInfo.domain.entity.StuInfoEntity;
import net.lab1024.smartadmin.module.business.userInfo.domain.vo.StuInfoVO;
import net.lab1024.smartadmin.module.business.userInfo.domain.vo.StuInfoExcelVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:15:05
 * @since JDK1.8
 */
@Mapper
@Component
public interface StuInfoDao extends BaseMapper<StuInfoEntity> {

    /**
     * 分页查询
     * @param queryDTO
     * @return StuInfoVO
    */
    IPage<StuInfoVO> queryByPage(Page page, @Param("queryDTO") StuInfoQueryDTO queryDTO);

    /**
     * 根据id删除
     * @param id
     * @return
    */
    void deleteById(@Param("id")Long id);

    /**
     * 根据id批量删除
     * @param idList
     * @return
    */
    void deleteByIdList(@Param("idList") List<Long> idList);

    /**
     * 新增数据
     * @param queryDTO
     * @return
    */
    void insertStuInfo(@Param("queryDTO")StuInfoEntity queryDTO);

        /**
     * 查询所有导出数据
     * @param queryDTO
     * @return
     */
    List<StuInfoExcelVO> queryAllExportData(@Param("queryDTO")StuInfoQueryDTO queryDTO);

        /**
         * 查询批量导出数据
         * @param idList
         * @return
         */
    List<StuInfoExcelVO> queryBatchExportData(@Param("idList")List<Long> idList);
}
