package net.lab1024.smartadmin.module.business.chat.controller;

import net.lab1024.smartadmin.common.domain.PageResultDTO;
import net.lab1024.smartadmin.common.controller.BaseController;
import net.lab1024.smartadmin.common.domain.ResponseDTO;
import net.lab1024.smartadmin.common.domain.ValidateList;
import net.lab1024.smartadmin.module.business.chat.domain.dto.FriendsAddDTO;
import net.lab1024.smartadmin.module.business.chat.domain.dto.FriendsUpdateDTO;
import net.lab1024.smartadmin.module.business.chat.domain.dto.FriendsQueryDTO;
import net.lab1024.smartadmin.module.business.chat.domain.vo.FriendsVO;
import net.lab1024.smartadmin.module.business.chat.domain.vo.FriendsExcelVO;
import net.lab1024.smartadmin.module.business.chat.service.FriendsService;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import net.lab1024.smartadmin.module.business.userInfo.service.StuInfoService;
import org.apache.poi.ss.usermodel.Workbook;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:20:54
 * @since JDK1.8
 */
@RestController
@Api(tags = {""})
public class FriendsController extends BaseController {

    @Autowired
    private FriendsService friendsService;

    @Autowired
    private StuInfoService stuInfoService;

    @ApiOperation(value = "分页查询",notes = "@author linjh")
    @PostMapping("/friends/page/query")
    public ResponseDTO<PageResultDTO<FriendsVO>> queryByPage(@RequestBody FriendsQueryDTO queryDTO) {
        return friendsService.queryByPage(queryDTO);
    }

    @ApiOperation(value = "添加",notes = "@author linjh")
    @PostMapping("/friends/add")
    public ResponseDTO<String> add(@RequestBody @Validated FriendsAddDTO addTO){
        return friendsService.add(addTO);
    }

    @ApiOperation(value="修改",notes = "@author linjh")
    @PostMapping("/friends/update")
    public ResponseDTO<String> update(@RequestBody @Validated FriendsUpdateDTO updateDTO){
        return friendsService.update(updateDTO);
    }

    @ApiOperation(value="批量删除",notes = "@author linjh")
    @PostMapping("/friends/deleteByIds")
    public ResponseDTO<String> delete(@RequestBody @Validated ValidateList<Long> idList) {
        return friendsService.deleteByIds(idList);
    }

    @ApiOperation(value = "批量导出", notes = "@author linjh")
    @PostMapping("/friends/export/batch")
    public void batchExport(@RequestBody @Validated ValidateList<Long> idList, HttpServletResponse response) {
        //查询数据
        List<FriendsExcelVO> friendsList = friendsService.queryBatchExportData(idList);
        //导出操作
        ExportParams ex = new ExportParams("", "Sheet1");
        Workbook workbook = ExcelExportUtil.exportExcel(ex, FriendsExcelVO.class, friendsList);
        downloadExcel("", workbook, response);
    }

    @ApiOperation(value = "导出全部", notes = "@author linjh")
    @PostMapping("/friends/export/all")
    public void exportAll(@RequestBody @Validated FriendsQueryDTO queryDTO, HttpServletResponse response) {
        //查询数据
        List<FriendsExcelVO> friendsList = friendsService.queryAllExportData(queryDTO);
        //导出操作
        ExportParams ex = new ExportParams("", "Sheet1");
        Workbook workbook = ExcelExportUtil.exportExcel(ex, FriendsExcelVO.class, friendsList);
        downloadExcel("", workbook, response);
    }

}
