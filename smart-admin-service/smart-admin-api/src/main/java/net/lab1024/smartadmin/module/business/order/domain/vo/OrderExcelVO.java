package net.lab1024.smartadmin.module.business.order.domain.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import java.util.Date;

/**
 *  [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-03-08 23:25:59
 * @since JDK1.8
 */
@Data
public class OrderExcelVO {
    @Excel(name = "id")
    private Long id;

    @Excel(name = "order_name")
    private String orderName;

    @Excel(name = "order_address")
    private String orderAddress;

    @Excel(name = "order_info")
    private String orderInfo;

    @Excel(name = "order_money")
    private String orderMoney;

    @Excel(name = "order_type")
    private String orderType;

    @Excel(name = "create_id")
    private String createId;

    @Excel(name = "sign_id")
    private String signId;

    @Excel(name = "sign_status")
    private Integer signStatus;

    @Excel(name = "arr_status")
    private Integer arrStatus;

    @Excel(name = "order_status")
    private Integer orderStatus;

    @Excel(name = "order_img_url")
    private String orderImgUrl;

    @Excel(name = "arr_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date arrTime;

    @Excel(name = "arr_address")
    private String arrAddress;

    @Excel(name = "remuneration_money")
    private String remunerationMoney;

    @Excel(name = "order_Latitude_longitude")
    private String orderLatitudeLongitude;

    @Excel(name = "arr_Latitude_longitude")
    private String arrLatitudeLongitude;

    @Excel(name = "create_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Excel(name = "update_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;



}
