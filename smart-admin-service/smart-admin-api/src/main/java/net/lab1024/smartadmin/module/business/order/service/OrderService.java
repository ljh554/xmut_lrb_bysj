package net.lab1024.smartadmin.module.business.order.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.lab1024.smartadmin.common.domain.PageResultDTO;
import net.lab1024.smartadmin.common.domain.ResponseDTO;
import net.lab1024.smartadmin.module.business.chat.dao.FriendsDao;
import net.lab1024.smartadmin.module.business.chat.dao.MsgDao;
import net.lab1024.smartadmin.module.business.chat.domain.vo.FriendsVO;
import net.lab1024.smartadmin.module.business.order.dao.OrderDao;
import net.lab1024.smartadmin.module.business.order.domain.dto.OrderAddDTO;
import net.lab1024.smartadmin.module.business.order.domain.dto.OrderUpdateDTO;
import net.lab1024.smartadmin.module.business.order.domain.dto.OrderQueryDTO;
import net.lab1024.smartadmin.module.business.order.domain.entity.OrderEntity;
import net.lab1024.smartadmin.module.business.order.domain.vo.HomeVo;
import net.lab1024.smartadmin.module.business.order.domain.vo.OrderVO;
import net.lab1024.smartadmin.module.business.order.domain.vo.OrderExcelVO;
import net.lab1024.smartadmin.util.SmartPageUtil;
import net.lab1024.smartadmin.util.SmartBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-03-08 23:25:59
 * @since JDK1.8
 */
@Service
public class OrderService {

    @Autowired
    private MsgDao msgDao;
    @Autowired
    private FriendsDao friendsDao;
    @Autowired
    private OrderDao orderDao;

    /**
     * 根据id查询
     */
    public OrderEntity getById(Long id){
        return  orderDao.selectById(id);
    }

    /**
     * 分页查询
     * @author linjh
     * @date 2021-03-08 23:25:59
     */
    public ResponseDTO<HomeVo> queryByHome(OrderQueryDTO queryDTO) {
        Page page = SmartPageUtil.convert2QueryPage(queryDTO);
        IPage<OrderVO> voList1 = orderDao.queryByPage(page, queryDTO);
        HomeVo HomeVo = new HomeVo();
        HomeVo.setNewOrder(voList1.getRecords().toArray().length);
        return ResponseDTO.succData(HomeVo);
    }

    /**
     * 查询首页数据
     * @author linjh
     * @date 2021-03-08 23:25:59
     */
    public ResponseDTO<PageResultDTO<OrderVO>> queryByPage(OrderQueryDTO queryDTO) {
        Page page = SmartPageUtil.convert2QueryPage(queryDTO);
        IPage<OrderVO> voList = orderDao.queryByPage(page, queryDTO);
        PageResultDTO<OrderVO> pageResultDTO = SmartPageUtil.convert2PageResult(voList);
        return ResponseDTO.succData(pageResultDTO);
    }

    /**
     * 添加
     * @author linjh
     * @date 2021-03-08 23:25:59
     */
    public ResponseDTO<String> add(OrderAddDTO addDTO) {
        OrderEntity entity = SmartBeanUtil.copy(addDTO, OrderEntity.class);
        orderDao.insert(entity);
        return ResponseDTO.succ();
    }

    /**
     * 编辑
     * @author linjh
     * @date 2021-03-08 23:25:59
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> update(OrderUpdateDTO updateDTO) {
        OrderEntity entity = SmartBeanUtil.copy(updateDTO, OrderEntity.class);
        orderDao.updateById(entity);
        return ResponseDTO.succ();
    }

    /**
     * 删除
     * @author linjh
     * @date 2021-03-08 23:25:59
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> deleteByIds(List<Long> idList) {
        orderDao.deleteByIdList(idList);
        return ResponseDTO.succ();
    }

    /**
     * 查询全部导出对象
     * @author linjh
     * @date 2021-03-08 23:25:59
     */
    public List<OrderExcelVO> queryAllExportData(OrderQueryDTO queryDTO) {
        return orderDao.queryAllExportData( queryDTO);
    }

    /**
     * 批量查询导出对象
     * @author linjh
     * @date 2021-03-08 23:25:59
     */
    public List<OrderExcelVO> queryBatchExportData(List<Long> idList) {
        return orderDao.queryBatchExportData(idList);
    }
}
