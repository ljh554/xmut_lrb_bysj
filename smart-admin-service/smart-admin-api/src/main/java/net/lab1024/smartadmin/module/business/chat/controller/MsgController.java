package net.lab1024.smartadmin.module.business.chat.controller;

import net.lab1024.smartadmin.common.domain.PageResultDTO;
import net.lab1024.smartadmin.common.controller.BaseController;
import net.lab1024.smartadmin.common.domain.ResponseDTO;
import net.lab1024.smartadmin.common.domain.ValidateList;
import net.lab1024.smartadmin.module.business.chat.domain.dto.MsgAddDTO;
import net.lab1024.smartadmin.module.business.chat.domain.dto.MsgUpdateDTO;
import net.lab1024.smartadmin.module.business.chat.domain.dto.MsgQueryDTO;
import net.lab1024.smartadmin.module.business.chat.domain.vo.MsgVO;
import net.lab1024.smartadmin.module.business.chat.domain.vo.MsgExcelVO;
import net.lab1024.smartadmin.module.business.chat.service.MsgService;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.apache.poi.ss.usermodel.Workbook;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:23:01
 * @since JDK1.8
 */
@RestController
@Api(tags = {""})
public class MsgController extends BaseController {

    @Autowired
    private MsgService msgService;

    @ApiOperation(value = "分页查询",notes = "@author linjh")
    @PostMapping("/msg/page/query")
    public ResponseDTO<PageResultDTO<MsgVO>> queryByPage(@RequestBody MsgQueryDTO queryDTO) {
        return msgService.queryByPage(queryDTO);
    }

    @ApiOperation(value = "添加",notes = "@author linjh")
    @PostMapping("/msg/add")
    public ResponseDTO<String> add(@RequestBody @Validated MsgAddDTO addTO){
        return msgService.add(addTO);
    }

    @ApiOperation(value="修改",notes = "@author linjh")
    @PostMapping("/msg/update")
    public ResponseDTO<String> update(@RequestBody @Validated MsgUpdateDTO updateDTO){
        return msgService.update(updateDTO);
    }

    @ApiOperation(value="批量删除",notes = "@author linjh")
    @PostMapping("/msg/deleteByIds")
    public ResponseDTO<String> delete(@RequestBody @Validated ValidateList<Long> idList) {
        return msgService.deleteByIds(idList);
    }

    @ApiOperation(value = "批量导出", notes = "@author linjh")
    @PostMapping("/msg/export/batch")
    public void batchExport(@RequestBody @Validated ValidateList<Long> idList, HttpServletResponse response) {
        //查询数据
        List<MsgExcelVO> msgList = msgService.queryBatchExportData(idList);
        //导出操作
        ExportParams ex = new ExportParams("", "Sheet1");
        Workbook workbook = ExcelExportUtil.exportExcel(ex, MsgExcelVO.class, msgList);
        downloadExcel("", workbook, response);
    }

    @ApiOperation(value = "导出全部", notes = "@author linjh")
    @PostMapping("/msg/export/all")
    public void exportAll(@RequestBody @Validated MsgQueryDTO queryDTO, HttpServletResponse response) {
        //查询数据
        List<MsgExcelVO> msgList = msgService.queryAllExportData(queryDTO);
        //导出操作
        ExportParams ex = new ExportParams("", "Sheet1");
        Workbook workbook = ExcelExportUtil.exportExcel(ex, MsgExcelVO.class, msgList);
        downloadExcel("", workbook, response);
    }

}
