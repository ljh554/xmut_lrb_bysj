package net.lab1024.smartadmin.module.business.userInfo.controller;

import net.lab1024.smartadmin.common.domain.PageResultDTO;
import net.lab1024.smartadmin.common.controller.BaseController;
import net.lab1024.smartadmin.common.domain.ResponseDTO;
import net.lab1024.smartadmin.common.domain.ValidateList;
import net.lab1024.smartadmin.module.business.userInfo.domain.dto.StuInfoAddDTO;
import net.lab1024.smartadmin.module.business.userInfo.domain.dto.StuInfoUpdateDTO;
import net.lab1024.smartadmin.module.business.userInfo.domain.dto.StuInfoQueryDTO;
import net.lab1024.smartadmin.module.business.userInfo.domain.vo.StuInfoVO;
import net.lab1024.smartadmin.module.business.userInfo.domain.vo.StuInfoExcelVO;
import net.lab1024.smartadmin.module.business.userInfo.service.StuInfoService;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.apache.poi.ss.usermodel.Workbook;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:15:05
 * @since JDK1.8
 */
@RestController
@Api(tags = {""})
public class StuInfoController extends BaseController {

    @Autowired
    private StuInfoService stuInfoService;

    @ApiOperation(value = "分页查询",notes = "@author linjh")
    @PostMapping("/stuInfo/page/query")
    public ResponseDTO<PageResultDTO<StuInfoVO>> queryByPage(@RequestBody StuInfoQueryDTO queryDTO) {
        return stuInfoService.queryByPage(queryDTO);
    }

    @ApiOperation(value = "添加",notes = "@author linjh")
    @PostMapping("/stuInfo/add")
    public ResponseDTO<String> add(@RequestBody @Validated StuInfoAddDTO addTO){
        return stuInfoService.add(addTO);
    }

    @ApiOperation(value="修改",notes = "@author linjh")
    @PostMapping("/stuInfo/update")
    public ResponseDTO<String> update(@RequestBody @Validated StuInfoUpdateDTO updateDTO){
        return stuInfoService.update(updateDTO);
    }

    @ApiOperation(value="批量删除",notes = "@author linjh")
    @PostMapping("/stuInfo/deleteByIds")
    public ResponseDTO<String> delete(@RequestBody @Validated ValidateList<Long> idList) {
        return stuInfoService.deleteByIds(idList);
    }

    @ApiOperation(value = "批量导出", notes = "@author linjh")
    @PostMapping("/stuInfo/export/batch")
    public void batchExport(@RequestBody @Validated ValidateList<Long> idList, HttpServletResponse response) {
        //查询数据
        List<StuInfoExcelVO> stuInfoList = stuInfoService.queryBatchExportData(idList);
        //导出操作
        ExportParams ex = new ExportParams("", "Sheet1");
        Workbook workbook = ExcelExportUtil.exportExcel(ex, StuInfoExcelVO.class, stuInfoList);
        downloadExcel("", workbook, response);
    }

    @ApiOperation(value = "导出全部", notes = "@author linjh")
    @PostMapping("/stuInfo/export/all")
    public void exportAll(@RequestBody @Validated StuInfoQueryDTO queryDTO, HttpServletResponse response) {
        //查询数据
        List<StuInfoExcelVO> stuInfoList = stuInfoService.queryAllExportData(queryDTO);
        //导出操作
        ExportParams ex = new ExportParams("", "Sheet1");
        Workbook workbook = ExcelExportUtil.exportExcel(ex, StuInfoExcelVO.class, stuInfoList);
        downloadExcel("", workbook, response);
    }

}
