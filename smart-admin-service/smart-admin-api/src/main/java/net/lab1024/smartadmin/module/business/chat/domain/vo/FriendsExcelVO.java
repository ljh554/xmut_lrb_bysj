package net.lab1024.smartadmin.module.business.chat.domain.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import java.util.Date;

/**
 *  [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-04-08 23:20:54
 * @since JDK1.8
 */
@Data
public class FriendsExcelVO {
    @Excel(name = "id")
    private Long id;

    @Excel(name = "用户id")
    private String userid;

    @Excel(name = "好友id")
    private String fuserid;

    @Excel(name = "create_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Excel(name = "update_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;



}
