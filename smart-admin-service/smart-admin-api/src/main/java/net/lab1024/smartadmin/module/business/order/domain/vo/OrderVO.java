package net.lab1024.smartadmin.module.business.order.domain.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

/**
 *  [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-03-08 23:25:59
 * @since JDK1.8
 */
@Data
public class OrderVO {
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("order_name")
    private String orderName;

    @ApiModelProperty("order_address")
    private String orderAddress;

    @ApiModelProperty("order_info")
    private String orderInfo;

    @ApiModelProperty("order_money")
    private String orderMoney;

    @Excel(name = "order_type")
    private String orderType;

    @ApiModelProperty("create_id")
    private String createId;

    @ApiModelProperty("sign_id")
    private String signId;

    @ApiModelProperty("sign_status")
    private Integer signStatus;

    @ApiModelProperty("arr_status")
    private Integer arrStatus;

    @ApiModelProperty("order_status")
    private Integer orderStatus;

    @ApiModelProperty("order_img_url")
    private String orderImgUrl;

    @ApiModelProperty("arr_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date arrTime;

    @ApiModelProperty("arr_address")
    private String arrAddress;

    @ApiModelProperty("remuneration_money")
    private String remunerationMoney;

    @ApiModelProperty("order_Latitude_longitude")
    private String orderLatitudeLongitude;

    @ApiModelProperty("arr_Latitude_longitude")
    private String arrLatitudeLongitude;

    @ApiModelProperty("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;



}
