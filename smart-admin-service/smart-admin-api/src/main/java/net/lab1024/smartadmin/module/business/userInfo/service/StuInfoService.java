package net.lab1024.smartadmin.module.business.userInfo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.lab1024.smartadmin.common.domain.PageResultDTO;
import net.lab1024.smartadmin.common.domain.ResponseDTO;
import net.lab1024.smartadmin.module.business.userInfo.dao.StuInfoDao;
import net.lab1024.smartadmin.module.business.userInfo.domain.dto.StuInfoAddDTO;
import net.lab1024.smartadmin.module.business.userInfo.domain.dto.StuInfoUpdateDTO;
import net.lab1024.smartadmin.module.business.userInfo.domain.dto.StuInfoQueryDTO;
import net.lab1024.smartadmin.module.business.userInfo.domain.entity.StuInfoEntity;
import net.lab1024.smartadmin.module.business.userInfo.domain.vo.StuInfoVO;
import net.lab1024.smartadmin.module.business.userInfo.domain.vo.StuInfoExcelVO;
import net.lab1024.smartadmin.module.system.employee.constant.EmployeeResponseCodeConst;
import net.lab1024.smartadmin.module.system.employee.constant.EmployeeStatusEnum;
import net.lab1024.smartadmin.module.system.employee.domain.dto.EmployeeDTO;
import net.lab1024.smartadmin.util.SmartPageUtil;
import net.lab1024.smartadmin.util.SmartBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:15:05
 * @since JDK1.8
 */
@Service
public class StuInfoService {

    @Autowired
    private StuInfoDao stuInfoDao;

    /**
     * 根据id查询
     */
    public StuInfoEntity getById(Long id){
        return  stuInfoDao.selectById(id);
    }

    /**
     * 分页查询
     * @author linjh
     * @date 2021-04-08 23:15:05
     */
    public ResponseDTO<PageResultDTO<StuInfoVO>> queryByPage(StuInfoQueryDTO queryDTO) {
        Page page = SmartPageUtil.convert2QueryPage(queryDTO);
        IPage<StuInfoVO> voList = stuInfoDao.queryByPage(page, queryDTO);
        PageResultDTO<StuInfoVO> pageResultDTO = SmartPageUtil.convert2PageResult(voList);
        return ResponseDTO.succData(pageResultDTO);
    }

    /**
     * 添加
     * @author linjh
     * @date 2021-04-08 23:15:05
     */
    public ResponseDTO<String> add(StuInfoAddDTO addDTO) {
        StuInfoEntity entity = SmartBeanUtil.copy(addDTO, StuInfoEntity.class);
        stuInfoDao.insert(entity);
        return ResponseDTO.succ();
    }

    /**
     * 编辑
     * @author linjh
     * @date 2021-04-08 23:15:05
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> update(StuInfoUpdateDTO updateDTO) {
        StuInfoEntity entity = SmartBeanUtil.copy(updateDTO, StuInfoEntity.class);
        StuInfoEntity sameIdEmployee = stuInfoDao.selectById(updateDTO.getId());
        if (null != sameIdEmployee) {
            stuInfoDao.updateById(entity);
        }else {
            stuInfoDao.insertStuInfo(entity);
        }
        return ResponseDTO.succ();
    }


    /**
     * 删除
     * @author linjh
     * @date 2021-04-08 23:15:05
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> deleteByIds(List<Long> idList) {
        stuInfoDao.deleteByIdList(idList);
        return ResponseDTO.succ();
    }

    /**
     * 查询全部导出对象
     * @author linjh
     * @date 2021-04-08 23:15:05
     */
    public List<StuInfoExcelVO> queryAllExportData(StuInfoQueryDTO queryDTO) {
        return stuInfoDao.queryAllExportData( queryDTO);
    }

    /**
     * 批量查询导出对象
     * @author linjh
     * @date 2021-04-08 23:15:05
     */
    public List<StuInfoExcelVO> queryBatchExportData(List<Long> idList) {
        return stuInfoDao.queryBatchExportData(idList);
    }
}
