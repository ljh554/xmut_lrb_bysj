package net.lab1024.smartadmin.module.business.chat.domain.dto;

import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

/**
 * 新建 [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 2018 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-04-08 23:23:01
 * @since JDK1.8
 */
@Data
public class MsgAddDTO {
    @ApiModelProperty("发送者id")
    private String senduserid;

    @ApiModelProperty("接收者id")
    private String reciveuserid;

    @ApiModelProperty("发送内容")
    private String sendtext;

    @ApiModelProperty("发送时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date date;

    @ApiModelProperty("消息类型")
    private String mine;

    @ApiModelProperty("name")
    private String name;

    @ApiModelProperty("img")
    private String img;

    @ApiModelProperty("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;


}
