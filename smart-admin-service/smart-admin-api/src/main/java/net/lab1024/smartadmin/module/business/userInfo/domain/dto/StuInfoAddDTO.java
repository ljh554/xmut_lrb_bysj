package net.lab1024.smartadmin.module.business.userInfo.domain.dto;

import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

/**
 * 新建 [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 2018 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-04-08 23:15:05
 * @since JDK1.8
 */
@Data
public class StuInfoAddDTO {
    @ApiModelProperty("头像")
    private String headImgUrl;

    @ApiModelProperty("校园卡图片")
    private String stuCardUrl;

    @ApiModelProperty("学号")
    private String stuCard;

    @ApiModelProperty("完成订单数量")
    private Integer sucOrderNum;

    @ApiModelProperty("签约订单数量")
    private Integer getOrderNum;

    @ApiModelProperty("发起订单数量")
    private Integer startOrderNum;

    @ApiModelProperty("取消订单数量")
    private Integer calOrderNum;

    @ApiModelProperty("押金金额")
    private Integer deposit;

    @ApiModelProperty("用户名")
    private String actualName;

    @ApiModelProperty("手机号码")
    private String phone;

    @ApiModelProperty("身份证")
    private String idCard;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("remark")
    private String remark;

    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


}
