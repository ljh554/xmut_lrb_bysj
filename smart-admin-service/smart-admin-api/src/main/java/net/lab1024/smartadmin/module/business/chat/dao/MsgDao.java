package net.lab1024.smartadmin.module.business.chat.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import net.lab1024.smartadmin.module.business.chat.domain.dto.MsgQueryDTO;
import net.lab1024.smartadmin.module.business.chat.domain.entity.MsgEntity;
import net.lab1024.smartadmin.module.business.chat.domain.vo.MsgVO;
import net.lab1024.smartadmin.module.business.chat.domain.vo.MsgExcelVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:23:01
 * @since JDK1.8
 */
@Mapper
@Component
public interface MsgDao extends BaseMapper<MsgEntity> {

    /**
     * 分页查询
     * @param queryDTO
     * @return MsgVO
    */
    IPage<MsgVO> queryByPage(Page page, @Param("queryDTO") MsgQueryDTO queryDTO);

    /**
     * 根据id删除
     * @param id
     * @return
    */
    void deleteById(@Param("id")Long id);

    /**
     * 根据id批量删除
     * @param idList
     * @return
    */
    void deleteByIdList(@Param("idList") List<Long> idList);

        /**
     * 查询所有导出数据
     * @param queryDTO
     * @return
     */
    List<MsgExcelVO> queryAllExportData(@Param("queryDTO")MsgQueryDTO queryDTO);

        /**
         * 查询批量导出数据
         * @param idList
         * @return
         */
    List<MsgExcelVO> queryBatchExportData(@Param("idList")List<Long> idList);
}
