package net.lab1024.smartadmin.module.business.order.domain.dto;

import net.lab1024.smartadmin.common.domain.PageParamDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-03-08 23:25:59
 * @since JDK1.8
 */
@Data
public class OrderQueryDTO extends PageParamDTO {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("order_name")
    private String orderName;

    @ApiModelProperty("order_address")
    private String orderAddress;

    @ApiModelProperty("create_id")
    private String createId;

    @ApiModelProperty("sign_id")
    private String signId;

    @ApiModelProperty("order_type")
    private String orderType;

    @ApiModelProperty("sign_status")
    private Integer signStatus;

    @ApiModelProperty("remuneration_money")
    private String remunerationMoney;

    @ApiModelProperty("order_money")
    private String orderMoney;

    @ApiModelProperty("创建时间-开始")
    private Date createTimeBegin;

    @ApiModelProperty("创建时间-截止")
    private Date createTimeEnd;

    @ApiModelProperty("上次更新时间-开始")
    private Date updateTimeBegin;

    @ApiModelProperty("上次更新创建时间-开始")
    private Date updateTimeEnd;
}
