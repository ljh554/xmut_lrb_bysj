package net.lab1024.smartadmin.module.business.userInfo.domain.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import java.util.Date;

/**
 *  [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-04-08 23:15:05
 * @since JDK1.8
 */
@Data
public class StuInfoExcelVO {
    @Excel(name = "用户id")
    private Long id;

    @Excel(name = "头像")
    private String headImgUrl;

    @Excel(name = "校园卡图片")
    private String stuCardUrl;

    @Excel(name = "学号")
    private String stuCard;

    @Excel(name = "完成订单数量")
    private Integer sucOrderNum;

    @Excel(name = "签约订单数量")
    private Integer getOrderNum;

    @Excel(name = "发起订单数量")
    private Integer startOrderNum;

    @Excel(name = "押金金额")
    private Integer deposit;

    @Excel(name = "用户名")
    private String actualName;

    @Excel(name = "手机号码")
    private String phone;

    @Excel(name = "身份证")
    private String idCard;

    @Excel(name = "邮箱")
    private String email;

    @Excel(name = "remark")
    private String remark;

    @Excel(name = "更新时间", format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @Excel(name = "创建时间", format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;



}
