package net.lab1024.smartadmin.module.business.chat.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.lab1024.smartadmin.common.domain.PageResultDTO;
import net.lab1024.smartadmin.common.domain.ResponseDTO;
import net.lab1024.smartadmin.module.business.chat.dao.MsgDao;
import net.lab1024.smartadmin.module.business.chat.domain.dto.MsgAddDTO;
import net.lab1024.smartadmin.module.business.chat.domain.dto.MsgUpdateDTO;
import net.lab1024.smartadmin.module.business.chat.domain.dto.MsgQueryDTO;
import net.lab1024.smartadmin.module.business.chat.domain.entity.MsgEntity;
import net.lab1024.smartadmin.module.business.chat.domain.vo.MsgVO;
import net.lab1024.smartadmin.module.business.chat.domain.vo.MsgExcelVO;
import net.lab1024.smartadmin.util.SmartPageUtil;
import net.lab1024.smartadmin.util.SmartBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:23:01
 * @since JDK1.8
 */
@Service
public class MsgService {

    @Autowired
    private MsgDao msgDao;

    /**
     * 根据id查询
     */
    public MsgEntity getById(Long id){
        return  msgDao.selectById(id);
    }

    /**
     * 分页查询
     * @author linjh
     * @date 2021-04-08 23:23:01
     */
    public ResponseDTO<PageResultDTO<MsgVO>> queryByPage(MsgQueryDTO queryDTO) {
        Page page = SmartPageUtil.convert2QueryPage(queryDTO);
        IPage<MsgVO> voList = msgDao.queryByPage(page, queryDTO);
        PageResultDTO<MsgVO> pageResultDTO = SmartPageUtil.convert2PageResult(voList);
        return ResponseDTO.succData(pageResultDTO);
    }

    /**
     * 添加
     * @author linjh
     * @date 2021-04-08 23:23:01
     */
    public ResponseDTO<String> add(MsgAddDTO addDTO) {
        MsgEntity entity = SmartBeanUtil.copy(addDTO, MsgEntity.class);
        msgDao.insert(entity);
        return ResponseDTO.succ();
    }

    /**
     * 编辑
     * @author linjh
     * @date 2021-04-08 23:23:01
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> update(MsgUpdateDTO updateDTO) {
        MsgEntity entity = SmartBeanUtil.copy(updateDTO, MsgEntity.class);
        msgDao.updateById(entity);
        return ResponseDTO.succ();
    }

    /**
     * 删除
     * @author linjh
     * @date 2021-04-08 23:23:01
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> deleteByIds(List<Long> idList) {
        msgDao.deleteByIdList(idList);
        return ResponseDTO.succ();
    }

    /**
     * 查询全部导出对象
     * @author linjh
     * @date 2021-04-08 23:23:01
     */
    public List<MsgExcelVO> queryAllExportData(MsgQueryDTO queryDTO) {
        return msgDao.queryAllExportData( queryDTO);
    }

    /**
     * 批量查询导出对象
     * @author linjh
     * @date 2021-04-08 23:23:01
     */
    public List<MsgExcelVO> queryBatchExportData(List<Long> idList) {
        return msgDao.queryBatchExportData(idList);
    }
}
