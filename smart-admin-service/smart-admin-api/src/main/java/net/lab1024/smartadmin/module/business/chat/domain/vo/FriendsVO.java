package net.lab1024.smartadmin.module.business.chat.domain.vo;

import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

/**
 *  [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-04-08 23:20:54
 * @since JDK1.8
 */
@Data
public class FriendsVO {
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("用户id")
    private String userid;

    @ApiModelProperty("好友id")
    private String fuserid;

    @ApiModelProperty("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;



}
