package net.lab1024.smartadmin.module.business.order.controller;

import net.lab1024.smartadmin.common.domain.PageResultDTO;
import net.lab1024.smartadmin.common.controller.BaseController;
import net.lab1024.smartadmin.common.domain.ResponseDTO;
import net.lab1024.smartadmin.common.domain.ValidateList;
import net.lab1024.smartadmin.module.business.order.domain.dto.OrderAddDTO;
import net.lab1024.smartadmin.module.business.order.domain.dto.OrderUpdateDTO;
import net.lab1024.smartadmin.module.business.order.domain.dto.OrderQueryDTO;
import net.lab1024.smartadmin.module.business.order.domain.vo.OrderVO;
import net.lab1024.smartadmin.module.business.order.domain.vo.OrderExcelVO;
import net.lab1024.smartadmin.module.business.order.service.OrderService;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.apache.poi.ss.usermodel.Workbook;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-03-08 23:25:59
 * @since JDK1.8
 */
@RestController
@Api(tags = {""})
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;

    @ApiOperation(value = "分页查询",notes = "@author linjh")
    @PostMapping("/order/page/query")
    public ResponseDTO<PageResultDTO<OrderVO>> queryByPage(@RequestBody OrderQueryDTO queryDTO) {
        return orderService.queryByPage(queryDTO);
    }

    @ApiOperation(value = "添加",notes = "@author linjh")
    @PostMapping("/order/add")
    public ResponseDTO<String> add(@RequestBody @Validated OrderAddDTO addTO){
        return orderService.add(addTO);
    }

    @ApiOperation(value="修改",notes = "@author linjh")
    @PostMapping("/order/update")
    public ResponseDTO<String> update(@RequestBody @Validated OrderUpdateDTO updateDTO){
        return orderService.update(updateDTO);
    }

    @ApiOperation(value="批量删除",notes = "@author linjh")
    @PostMapping("/order/deleteByIds")
    public ResponseDTO<String> delete(@RequestBody @Validated ValidateList<Long> idList) {
        return orderService.deleteByIds(idList);
    }

    @ApiOperation(value = "批量导出", notes = "@author linjh")
    @PostMapping("/order/export/batch")
    public void batchExport(@RequestBody @Validated ValidateList<Long> idList, HttpServletResponse response) {
        //查询数据
        List<OrderExcelVO> orderList = orderService.queryBatchExportData(idList);
        //导出操作
        ExportParams ex = new ExportParams("", "Sheet1");
        Workbook workbook = ExcelExportUtil.exportExcel(ex, OrderExcelVO.class, orderList);
        downloadExcel("", workbook, response);
    }

    @ApiOperation(value = "导出全部", notes = "@author linjh")
    @PostMapping("/order/export/all")
    public void exportAll(@RequestBody @Validated OrderQueryDTO queryDTO, HttpServletResponse response) {
        //查询数据
        List<OrderExcelVO> orderList = orderService.queryAllExportData(queryDTO);
        //导出操作
        ExportParams ex = new ExportParams("", "Sheet1");
        Workbook workbook = ExcelExportUtil.exportExcel(ex, OrderExcelVO.class, orderList);
        downloadExcel("", workbook, response);
    }

}
