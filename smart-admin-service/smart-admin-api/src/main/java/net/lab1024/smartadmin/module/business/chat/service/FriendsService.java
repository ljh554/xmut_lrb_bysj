package net.lab1024.smartadmin.module.business.chat.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.lab1024.smartadmin.common.domain.PageResultDTO;
import net.lab1024.smartadmin.common.domain.ResponseDTO;
import net.lab1024.smartadmin.module.business.chat.dao.FriendsDao;
import net.lab1024.smartadmin.module.business.chat.domain.dto.FriendsAddDTO;
import net.lab1024.smartadmin.module.business.chat.domain.dto.FriendsUpdateDTO;
import net.lab1024.smartadmin.module.business.chat.domain.dto.FriendsQueryDTO;
import net.lab1024.smartadmin.module.business.chat.domain.entity.FriendsEntity;
import net.lab1024.smartadmin.module.business.chat.domain.vo.FriendsVO;
import net.lab1024.smartadmin.module.business.chat.domain.vo.FriendsExcelVO;
import net.lab1024.smartadmin.module.business.order.domain.dto.OrderQueryDTO;
import net.lab1024.smartadmin.module.business.order.domain.vo.HomeVo;
import net.lab1024.smartadmin.module.business.order.domain.vo.OrderVO;
import net.lab1024.smartadmin.util.SmartPageUtil;
import net.lab1024.smartadmin.util.SmartBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:20:54
 * @since JDK1.8
 */
@Service
public class FriendsService {

    @Autowired
    private FriendsDao friendsDao;

    /**
     * 根据id查询
     */
    public FriendsEntity getById(Long id){
        return  friendsDao.selectById(id);
    }

    /**
     * 分页查询
     * @author linjh
     * @date 2021-04-08 23:20:54
     */
    public ResponseDTO<PageResultDTO<FriendsVO>> queryByPage(FriendsQueryDTO queryDTO) {
        Page page = SmartPageUtil.convert2QueryPage(queryDTO);
        IPage<FriendsVO> voList = friendsDao.queryByPage(page, queryDTO);
        PageResultDTO<FriendsVO> pageResultDTO = SmartPageUtil.convert2PageResult(voList);
        return ResponseDTO.succData(pageResultDTO);
    }

    /**
     * 添加
     * @author linjh
     * @date 2021-04-08 23:20:54
     */
    public ResponseDTO<String> add(FriendsAddDTO addDTO) {
        FriendsEntity entity = SmartBeanUtil.copy(addDTO, FriendsEntity.class);
        friendsDao.insert(entity);
        return ResponseDTO.succ();
    }

    /**
     * 编辑
     * @author linjh
     * @date 2021-04-08 23:20:54
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> update(FriendsUpdateDTO updateDTO) {
        FriendsEntity entity = SmartBeanUtil.copy(updateDTO, FriendsEntity.class);
        friendsDao.updateById(entity);
        return ResponseDTO.succ();
    }

    /**
     * 删除
     * @author linjh
     * @date 2021-04-08 23:20:54
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> deleteByIds(List<Long> idList) {
        friendsDao.deleteByIdList(idList);
        return ResponseDTO.succ();
    }

    /**
     * 查询全部导出对象
     * @author linjh
     * @date 2021-04-08 23:20:54
     */
    public List<FriendsExcelVO> queryAllExportData(FriendsQueryDTO queryDTO) {
        return friendsDao.queryAllExportData( queryDTO);
    }

    /**
     * 批量查询导出对象
     * @author linjh
     * @date 2021-04-08 23:20:54
     */
    public List<FriendsExcelVO> queryBatchExportData(List<Long> idList) {
        return friendsDao.queryBatchExportData(idList);
    }
}
