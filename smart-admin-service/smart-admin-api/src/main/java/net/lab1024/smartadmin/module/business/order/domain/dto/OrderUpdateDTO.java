package net.lab1024.smartadmin.module.business.order.domain.dto;

import lombok.Data;

/**
 * 更新 [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 2018 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-03-08 23:25:59
 * @since JDK1.8
 */
@Data
public class OrderUpdateDTO extends OrderAddDTO {

    private Long id;

}
