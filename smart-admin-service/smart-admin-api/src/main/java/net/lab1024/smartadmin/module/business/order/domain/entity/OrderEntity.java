package net.lab1024.smartadmin.module.business.order.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.lab1024.smartadmin.common.domain.BaseEntity;
import java.util.Date;
import lombok.Data;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-03-08 23:25:59
 * @since JDK1.8
 */
@Data
@TableName("t_order")
public class OrderEntity extends BaseEntity{


    /**
     * order_name
     */
    private String orderName;

    /**
     * order_address
     */
    private String orderAddress;

    /**
     * order_info
     */
    private String orderInfo;

    /**
     * order_money
     */
    private String orderMoney;

    /**
     * order_type
     */
    private String orderType;

    /**
     * create_id
     */
    private String createId;

    /**
     * sign_id
     */
    private String signId;

    /**
     * sign_status
     */
    private Integer signStatus;

    /**
     * arr_status
     */
    private Integer arrStatus;

    /**
     * order_status
     */
    private Integer orderStatus;

    /**
     * order_img_url
     */
    private String orderImgUrl;

    /**
     * arr_time
     */
    private Date arrTime;

    /**
     * arr_address
     */
    private String arrAddress;

    /**
     * remuneration_money
     */
    private String remunerationMoney;

    /**
     * order_Latitude_longitude
     */
    private String orderLatitudeLongitude;

    /**
     * arr_Latitude_longitude
     */
    private String arrLatitudeLongitude;



}
