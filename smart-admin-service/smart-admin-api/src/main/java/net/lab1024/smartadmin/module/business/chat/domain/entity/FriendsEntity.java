package net.lab1024.smartadmin.module.business.chat.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.lab1024.smartadmin.common.domain.BaseEntity;
import lombok.Data;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:20:54
 * @since JDK1.8
 */
@Data
@TableName("chat_friends")
public class FriendsEntity extends BaseEntity{


    /**
     * 用户id
     */
    private String userid;

    /**
     * 好友id
     */
    private String fuserid;



}
