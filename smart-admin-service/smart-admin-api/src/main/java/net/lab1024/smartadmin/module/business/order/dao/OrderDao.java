package net.lab1024.smartadmin.module.business.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import net.lab1024.smartadmin.module.business.order.domain.dto.OrderQueryDTO;
import net.lab1024.smartadmin.module.business.order.domain.entity.OrderEntity;
import net.lab1024.smartadmin.module.business.order.domain.vo.OrderVO;
import net.lab1024.smartadmin.module.business.order.domain.vo.OrderExcelVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-03-08 23:25:59
 * @since JDK1.8
 */
@Mapper
@Component
public interface OrderDao extends BaseMapper<OrderEntity> {

    /**
     * 分页查询
     * @param queryDTO
     * @return OrderVO
    */
    IPage<OrderVO> queryByPage(Page page, @Param("queryDTO") OrderQueryDTO queryDTO);

    /**
     * 根据id删除
     * @param id
     * @return
    */
    void deleteById(@Param("id")Long id);

    /**
     * 根据id批量删除
     * @param idList
     * @return
    */
    void deleteByIdList(@Param("idList") List<Long> idList);

        /**
     * 查询所有导出数据
     * @param queryDTO
     * @return
     */
    List<OrderExcelVO> queryAllExportData(@Param("queryDTO")OrderQueryDTO queryDTO);

        /**
         * 查询批量导出数据
         * @param idList
         * @return
         */
    List<OrderExcelVO> queryBatchExportData(@Param("idList")List<Long> idList);
}
