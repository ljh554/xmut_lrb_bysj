package net.lab1024.smartadmin.module.business.userInfo.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.lab1024.smartadmin.common.domain.BaseEntity;
import lombok.Data;

/**
 * [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c)  校园懒人帮系统Inc. All rights reserved.
 * @date 2021-04-08 23:15:05
 * @since JDK1.8
 */
@Data
@TableName("t_stu_info")
public class StuInfoEntity extends BaseEntity{

    /**
     * 头像
     */
    private Long id;
    /**
     * 头像
     */
    private String headImgUrl;

    /**
     * 校园卡图片
     */
    private String stuCardUrl;

    /**
     * 学号
     */
    private String stuCard;

    /**
     * 完成订单数量
     */
    private Integer sucOrderNum;

    /**
     * 签约订单数量
     */
    private Integer getOrderNum;

    /**
     * 发起订单数量
     */
    private Integer startOrderNum;

    /**
     * 发起订单数量
     */
    private Integer calOrderNum;

    /**
     * 押金金额
     */
    private Integer deposit;

    /**
     * 用户名
     */
    private String actualName;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 身份证
     */
    private String idCard;

    /**
     * 邮箱
     */
    private String email;

    /**
     * remark
     */
    private String remark;



}
