package net.lab1024.smartadmin.module.business.chat.domain.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import java.util.Date;

/**
 *  [  ]
 *
 * @author linjh
 * @version 1.0
 * @company 校园懒人帮系统
 * @copyright (c) 校园懒人帮系统Inc. All rights reserved.
 * @date  2021-04-08 23:23:01
 * @since JDK1.8
 */
@Data
public class MsgExcelVO {
    @Excel(name = "发送者id")
    private String senduserid;

    @Excel(name = "接收者id")
    private String reciveuserid;

    @Excel(name = "发送内容")
    private String sendtext;

    @Excel(name = "发送时间", format = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    @Excel(name = "消息类型")
    private String mine;

    @Excel(name = "name")
    private String name;

    @Excel(name = "img")
    private String img;

    @Excel(name = "create_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Excel(name = "update_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;



}
