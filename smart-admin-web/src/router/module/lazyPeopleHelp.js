import Main from '@/components/main';
// 首页
export const lazyPeopleHelp = [
  {
    path: '/lazyPeopleHelp',
    name: 'LazyPeopleHelp',
    component: Main,
    meta: {
      title: '懒人帮',
      icon: 'icon iconfont iconrenyuanguanli'
    },
    children: [
      {
        path: '/lazyPeopleHelp/list',
        name: 'LazyPeopleHelpList',
        meta: {
          title: '懒人帮',
          icon: 'icon iconfont iconrenyuanguanli',
          noValidatePrivilege: true,
          noKeepAlive: true
        },
        component: () => import('@/views/lazyPeopleHelp')
      }
    ]
  }
];
