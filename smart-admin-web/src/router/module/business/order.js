import Main from '@/components/main';
// t_order路由
export const order = [
    {
        path: '/order',
        name: 'Order',
        component: Main,
        meta: {
            title: '系统管理',
            icon: 'icon iconfont iconyoujianguanli'
        },
        children: [
            //  列表
            {
                path: '/order/order-list',
                name: 'OrderList',
                meta: {
                    title: '订单管理',
                    privilege: [
                        { title: '查询', name: 'order-list-query' },
                        { title: '新增', name: 'order-list-add' },
                        { title: '编辑', name: 'order-list-update' },
                        { title: '批量删除', name: 'order-list-batch-delete' },
                        { title: '批量导出', name: 'order-list-batch-export' },
                        { title: '导出全部', name: 'order-list-export-all' }
                    ]
                },
                component: () => import('@/views/business/order/order-list.vue')
             },
            //  列表
            {
                path: '/stu-info/stu-info-list',
                name: 'StuInfoList',
                meta: {
                    title: '人员管理',
                    privilege: [
                        { title: '查询', name: 'stu-info-list-query' }, 
                        { title: '新增', name: 'stu-info-list-add' },
                        { title: '编辑', name: 'stu-info-list-update' },
                        { title: '批量删除', name: 'stu-info-list-batch-delete' },
                        { title: '批量导出', name: 'stu-info-list-batch-export' },
                        { title: '导出全部', name: 'stu-info-list-export-all' } 
                    ]
                },
                component: () => import('@/views/business/stu-info/stu-info-list.vue')
            }
        ]
    }
];
