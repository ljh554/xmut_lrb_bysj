import Main from '@/components/main';
// t_order路由
export const chat = [
    {
        path: '/chat',
        name: 'chat',
        component: Main,
        meta: {
            title: '聊天中心',
            icon: 'icon iconfont iconyoujianguanli'
        },
        children: [
            //  列表
            {
                path: '/chat/chat-center',
                name: 'chatCenter',
                meta: {
                    title: '聊天中心',
                },
                component: () => import('@/views/business/chat/chatIndex.vue')
            }
        ]
    }
];
