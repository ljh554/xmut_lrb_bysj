import Main from '@/components/main';
// t_order路由
export const appointOrder = [
    {
        path: '/appointOrder',
        name: 'appointOrder',
        component: Main,
        meta: {
            title: '接单中心',
            icon: 'icon iconfont iconyoujianguanli'
        },
        children: [
            //  列表
            {
                path: '/appointOrder/appoint-order-list',
                name: 'appointOrderList',
                meta: {
                    title: '接单大厅',
                    privilege: [
                        { title: '查询', name: 'appoint-order-list-query' },
                        { title: '新增', name: 'appoint-order-list-add' },
                        { title: '编辑', name: 'appoint-order-list-update' },
                        { title: '批量删除', name: 'appoint-order-list-batch-delete' },
                        { title: '批量导出', name: 'appoint-order-list-batch-export' },
                        { title: '导出全部', name: 'appoint-order-list-export-all' }
                    ]
                },
                component: () => import('@/views/business/appointOrder/order-list.vue')
             },
          {
            path: '/appointOrder/my-order-list',
            name: 'myOrderList',
            meta: {
              title: '我的订单'
            },
            component: () => import('@/views/business/appointOrder/my-order.vue')
          },
          {
            path: '/appointOrder/create-order',
            name: 'createOrder',
            meta: {
              title: '创建订单',
              noValidatePrivilege: true,
            },
            component: () => import('@/views/business/appointOrder/components/create-order.vue')
          },
          {
            path: '/appointOrder/order-info',
            name: 'orderInfo',
            meta: {
              title: '订单详情',
              noValidatePrivilege: true,
            },
            component: () => import('@/views/business/appointOrder/components/order-info.vue')
          }
        ]
    }
];
