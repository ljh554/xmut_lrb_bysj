import Main from '@/components/main';
// t_order路由
export const personalCenter = [
  {
    path: '/personalCenter',
    name: 'personalCenter',
    component: Main,
    meta: {
      title: '用户中心',
      icon: 'icon iconfont iconyoujianguanli'
    },
    children: [
      //  列表
      {
        path: '/personalCenter/person-index',
        name: 'personalIndex',
        meta: {
          title: '用户中心',
        },
        component: () => import('@/views/business/personalCenter/personIndex.vue')
      }
    ]
  }
];
