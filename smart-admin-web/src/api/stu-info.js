import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const stuInfoApi = {
    // 添加 @author linjh
    addStuInfo: (data) => {
        return postAxios('/stuInfo/add', data);
    },
    // 分页查询 @author linjh
    queryStuInfo: (data) => {
        return postAxios('/stuInfo/page/query', data);
    },
    // 批量删除 @author linjh
    batchDeleteStuInfo: (idList) => {
        return postAxios('/stuInfo/deleteByIds', idList);
    },
    // 修改  @author linjh
    updateStuInfo: (data) => {
        return postAxios('/stuInfo/update',data);
    },
    // 导出全部  @author linjh
    exportAll:(data)=>{
        return postDownloadAxios('/stuInfo/export/all',data);
    },
    // 批量导出  @author linjh
    batchExport: (idList) => {
        return postDownloadAxios('/stuInfo/export/batch', idList);
    },
    queryOrder: (data) => {
        return postAxios('/order/page/query', data);
    },
};
