import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const orderApi = {
    // 添加 @author linjh
    addOrder: (data) => {
        return postAxios('/order/add', data);
    },
    // 分页查询 @author linjh
    queryOrder: (data) => {
        return postAxios('/order/page/query', data);
    },
    // 批量删除 @author linjh
    batchDeleteOrder: (idList) => {
        return postAxios('/order/deleteByIds', idList);
    },
    // 修改  @author linjh
    updateOrder: (data) => {
        return postAxios('/order/update',data);
    },
    // 导出全部  @author linjh
    exportAll:(data)=>{
        return postDownloadAxios('/order/export/all',data);
    },
    // 批量导出  @author linjh
    batchExport: (idList) => {
        return postDownloadAxios('/order/export/batch', idList);
    },
    addFriends: (data) => {
        return postAxios('/friends/add', data);
    },
};
