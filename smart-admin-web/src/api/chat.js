import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const chatApi = {
    // 分页查询 @author linjh
    queryMsg: (data) => {
        return postAxios('/msg/page/query', data);
    },
    // 分页查询 @author linjh
    queryFriends: (data) => {
        return postAxios('/friends/page/query', data);
    },
    querystuInfo: (data) => {
        return postAxios('/stuInfo/page/query', data);
    },
    // 添加 @author linjh
    addFriends: (data) => {
        return postAxios('/friends/add', data);
    },
};
